import { useEffect, useState } from "react";
import { json } from "@remix-run/node";
import { useActionData, useSubmit } from "@remix-run/react";
import {
  Page,
  Layout,
  Card,
  BlockStack,
  Box,
  TextField,
} from "@shopify/polaris";
import { authenticate } from "../shopify.server";
import Product from "../Helper/Product";



export const action = async ({ request }) => {
  const { admin } = await authenticate.admin(request);

  const product = await admin.graphql(`
  {
    products(first: 100) {
      nodes {
        title
        description
       tags 
       images(first:1) {
          edges {
            node {
              id
              originalSrc
              transformedSrc
            }
          }
        }
      }
    }
  }`);
  const {
    data: {
      products: { nodes },
    },
  } = await product.json();

  return json(nodes);
};
export default function Index() {
  const actionData = useActionData();
  const submit = useSubmit();

  const [data, setData] = useState([]);
  const [input, setInput] = useState("");
  const [dataFiltered, setDataFiltered] = useState([]);
  // setting initial data
  useEffect(() => {
    setData(actionData);
    setDataFiltered(actionData);
  }, [actionData]);
  // initially getting data
  useEffect(() => {
    submit({}, { replace: true, method: "POST" });
  }, []);
//  triggering funtion for filter when input updated
  useEffect(() => {
    let data1 = filterByTags(input.split(","), data);
    setDataFiltered(data1);
  }, [input]);
  // filtering data for tags
  function filterByTags(inputTags, products) {
    if (
      inputTags.length === 0 ||
      (inputTags.length === 1 && inputTags[0] == "")
    ) {
      return products;
    }

    return products.filter((product) => {
      const productTags = product.tags || "";
      return inputTags.some((tag) => productTags.includes(tag));
    });
  }

  return (
    <Page>
      <ui-title-bar title="Product Dashboard"></ui-title-bar>
      <BlockStack gap="500">
        <Layout>
          <Layout.Section>
            <Card padding={500}>
              <TextField
                value={input}
                onChange={(e) => {
                  setInput(e);
                }}
                label="Search by Tags"
                type="text"
                autoComplete="off"
                placeholder="Filter list using a comma seperated tags"
              />
              <Box background="bg-fill-info" style={{ marginTop: 50 }}>
                {dataFiltered &&
                  dataFiltered.map((f, i) => <Product key={i} item={f} />)}
              </Box>
            </Card>
          </Layout.Section>
        </Layout>
      </BlockStack>
    </Page>
  );
}
