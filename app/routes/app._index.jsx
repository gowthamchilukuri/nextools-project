import { useEffect, useReducer, useRef, useState } from "react";
import { json } from "@remix-run/node";
import { useActionData, useSubmit } from "@remix-run/react";
import {
  Page,
  Layout,
  Card,
  Button,
  BlockStack,
  InlineStack,
  Form,
  FormLayout,
  TextField,
  InlineGrid,
  Text,
} from "@shopify/polaris";
import { authenticate } from "../shopify.server";
import {
  initialState,
  reducer,
  actionTypes,
  createproductMutation,
} from "../Helper/Reducer";
export const loader = async ({ request }) => {
  await authenticate.admin(request);

  return null;
};

export const action = async ({ request }) => {
  const { admin, session } = await authenticate.admin(request);
  const body = await request.formData();
  let payload = JSON.parse(body.get("data"));

  const response = await admin.graphql(createproductMutation, payload);
  const responseJson = await response.json();
  const image = new admin.rest.resources.Image({ session: session });

  let img = body.get("image");
  let imageName = body.get("imagename");
  // only attching image when user attached it
  if (img && imageName) {
    let id = responseJson.data.productCreate.product.id.split("/");
   
    image.product_id = id[id.length - 1];
    image.position = 1;
    image.metafields = [
      {
        key: "new",
        value: "newvalue",
        type: "single_line_text_field",
        namespace: "global",
      },
    ];
    image.attachment = img.split(",")[1];
    image.filename = imageName;
    image.alt = imageName;
    await image.save({
      update: true,
    });
  }

  return json({
    product: responseJson,
  });
};

export default function Index() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [img, setimg] = useState({
    base64textString: "",
    imageName: "",
    showImage: false,
  });
  const actionData = useActionData();
  const submit = useSubmit();
  const fileInputRef = useRef(null);

// for showing err or product created
  useEffect(() => {
 if(actionData){  
    if (actionData?.product?.data?.productCreate?.product?.id) {
      shopify.toast.show("Product created");
    
      dispatch({
        type: actionTypes.RESET_STATE,
        payload: initialState,
      });
      setimg( {
        base64textString: "",
        imageName: "",
        showImage: false,
      })
    }
    else if(!actionData?.product?.data?.productCreate?.product) {
      shopify.toast.show("Error in creating product Please try again");

    }}
    
  }, [actionData]);
  // creating new product using state from reducer
  const createNewProduct = (event) => {
    event.preventDefault();
    const data = new FormData();
    let statedata = state;
    statedata.variables.input.tags =
      state.variables?.input?.tags == ""
        ? []
        : state.variables?.input?.tags?.split(",");
    let errorMessages = [];

    if (state.variables.input.title == "") {
      errorMessages.push("Required Title");
    }
    if (state.variables.input.metafields[0].key == "") {
      errorMessages.push("Required Metafield");
    }

    if (state.variables.input.metafields[0].value == "") {
      errorMessages.push("Required Metafield value");
    }
    if (
      state.variables.input.metafields[0].key.length < 3 &&
      state.variables.input.metafields[0].value != ""
    ) {
      errorMessages.push("Metafield key should be more than 3 characters");
    }

    if (errorMessages.length > 0) {
      shopify.toast.show(errorMessages.join(", "));
      return;
    }

    data.append("data", JSON.stringify(state));
    if (img) {
      data.append("image", img.base64textString);
      data.append("imagename", img.imageName);
    }

    submit(data, { replace: true, method: "POST" });
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error) => {
        reject(error);
      };

      fileReader.readAsDataURL(file);
    });
  };
  // processing img after selecting
  const handleFileChange = async (event) => {
    const file = event.target.files[0];
    if (file) {
      try {
        const base64Image = await convertBase64(file);

        setimg({
          base64textString: base64Image,
          imageName: file.name,
          showImage: true,
        });
      } catch (error) {
        console.error("Error converting image to base64:", error);
      }
    }
  };
  // for opening input for imageupload
  const handleButtonClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };
  return (
    <Page>
      <ui-title-bar title="Create New Product"></ui-title-bar>
      <BlockStack gap="500">
        <Layout>
          <Layout.Section>
            <Card padding={500}>
              <BlockStack gap="500">
                <Form onSubmit={createNewProduct}>
                  <FormLayout>
                    <InlineStack align="end">
                      <Button variant="primary" submit>
                        Save
                      </Button>
                    </InlineStack>
                    <TextField
                      value={state.variables.input.title}
                      onChange={(e) => {
                        dispatch({
                          type: actionTypes.UPDATE_TITLE,
                          payload: e.toUpperCase(),
                        });
                      }}
                      label="Product Title"
                      type="text"
                      autoComplete="off"
                      placeholder="Insert product title"
                    />
                    <TextField
                      value={state.variables.input.descriptionHtml}
                      onChange={(e) => {
                        dispatch({
                          type: actionTypes.UPDATE_DESCRIPTION,
                          payload: e,
                        });
                      }}
                      label="Product description"
                      type="text"
                      placeholder="Insert product description"
                      multiline={4}
                      autoComplete="off"
                    />
                    <TextField
                      value={state.variables.input.tags}
                      onChange={(e) => {
                        dispatch({
                          type: actionTypes.UPDATE_TAG,
                          payload: e,
                        });
                      }}
                      label="Product tags"
                      type="text"
                      placeholder="Insert a comma seperated list of tags"
                      autoComplete="off"
                    />

                    <InlineGrid columns={2} gap={400}>
                      <TextField
                        width="50%"
                        value={state.variables.input.metafields[0].key}
                        onChange={(e) => {
                          dispatch({
                            type: actionTypes.UPDATE_METAFIELD_KEY,
                            payload: e,
                          });
                        }}
                        label="MetaField Key"
                        type="text"
                        autoComplete="off"
                      />
                      <TextField
                        width="50%"
                        value={state.variables.input.metafields[0].value}
                        onChange={(e) => {
                          dispatch({
                            type: actionTypes.UPDATE_METAFIELD_VALUE,
                            payload: e,
                          });
                        }}
                        label="MetaField Value"
                        type="text"
                        autoComplete="off"
                      />
                    </InlineGrid>
                    <InlineStack align="start" blockAlign="center" gap={200}>
                      <input
                        ref={fileInputRef}
                        type="file"
                        style={{ display: "none" }}
                        accept="image/*" 
                        onChange={handleFileChange}
                      />

                      <Button  variant="primary" onClick={handleButtonClick}>
                        Upload Image
                      </Button>
                      {img?.imageName&&<Text >{img?.imageName}</Text>}
                    </InlineStack>
                  </FormLayout>
                </Form>
              </BlockStack>
            </Card>
          </Layout.Section>
        </Layout>
      </BlockStack>
    </Page>
  );
}
