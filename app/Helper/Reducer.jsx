
export const initialState = {
    variables: {
      input: {
        metafields: [
          {
            namespace: '',
            key: '',
            type: 'single_line_text_field',
            value: '',
          },
        ],
        tags: '',
        title: ``,
        descriptionHtml:''
      }
    },
  };
// Action types

export const actionTypes = {
    UPDATE_TITLE: 'UPDATE_TITLE',
    UPDATE_DESCRIPTION: 'UPDATE_DESCRIPTION',
    UPDATE_IMAGE: 'UPDATE_IMAGE',
    UPDATE_TAG: 'UPDATE_TAG',
    UPDATE_METAFIELD_KEY:'UPDATE_METAFIELD_KEY',
    UPDATE_METAFIELD_VALUE: 'UPDATE_METAFIELD_VALUE',
    RESET_STATE: 'RESET_STATE',
  };
// Reducer function
export const reducer  = (state, action) => {
  switch (action.type) {
    case 'UPDATE_TITLE':
      return { ...state, variables: { ...state.variables, input: { ...state.variables.input, title: action.payload } } };
    case 'UPDATE_DESCRIPTION':
      return { ...state, variables: { ...state.variables, input: { ...state.variables.input, descriptionHtml: action.payload } } };
    case 'UPDATE_IMAGE':
      return { ...state, variables: { ...state.variables, media: [...state.variables.media,{...action.payload}] } };
    case 'UPDATE_TAG':
      
      return { ...state, variables: { ...state.variables, input: { ...state.variables.input, tags: action.payload } } };
    case 'UPDATE_METAFIELD_KEY':
      let updatedkey ={ ...state.variables.input.metafields[0], key: action.payload}
      return {
        ...state,
        variables: {
          ...state.variables,
          input: {
            ...state.variables.input,
            metafields: [ updatedkey]
            
            
          },
        },
      };
      case 'UPDATE_METAFIELD_VALUE':
        let updated={ ...state.variables.input.metafields[0], value: action.payload}
        return {
          ...state,
          variables: {
            ...state.variables,
            input: {
              ...state.variables.input,
              metafields: [updated ]
              
              
            },
          },
        };
    case 'RESET_STATE':
      return action.payload; // Reset to the initial state
    default:
      return state;
  }
};

export const createproductMutation=  `#graphql
mutation CreateProductWithNewMedia($input: ProductInput!) {
  productCreate(input: $input) {
    product {
      id
      title
      metafields(first: 3) {
        edges {
          node {
            id
            namespace
            key
            value
          }
        }
      }
    
     
    }
    userErrors {
      field
      message
    }
  }
}`


