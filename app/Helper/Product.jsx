import React from "react";
import { Text, Card, Box, InlineStack } from "@shopify/polaris";
const Product = ({ item }) => {
  return (
    <Card>
      <InlineStack blockAlign="center"  gap={500} wrap={false}>
        <Box
          style={{
            width: "20%",
            height: 120,
            display: "flex",
            flexDirection: "column",
            alignItems: "start",
          }}
        >
          <img
            alt={item?.image?.alt}
            width="100%"
            height="100%"
            style={{
              objectFit: "cover",
              objectPosition: "center",
              borderRadius:20
            }}
            src={item?.images?.edges[0]?.node?.originalSrc}
          />
        </Box>
        <Box
          style={{
            width: "60%",
            display: "flex",
            flexDirection: "column",
            alignItems: "start",
            height: "120px",
            overFlow: "hidden",
          }}
        >
          <Text variant="headingLg" as="h1">
            {item?.title}
          </Text>
          <Text variant="bodyMd" as="p">
            {item?.description}
          </Text>
        </Box>
        <Box
          style={{
            width: "20%",
            display: "flex",
            flexDirection: "column",
            alignItems: "start",
            height: "120px",
          }}
        >
          <Text variant="headingLg" as="h2">
            {item?.tags ? "Tags" : ""}
          </Text>
          {item?.tags.map(f=><Text variant="bodyMd" as="p">
          {f},
          </Text>)
          }
        </Box>
      </InlineStack>
    </Card>
  );
};

export default Product;
