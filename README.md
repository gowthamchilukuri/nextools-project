# Nextools Challenge

Nextools Challenge to Create New Products app for shopify Store

## Project Description
In this project, You can create new products to your shopify store by filling the form, and you can view the list of products in the dasboard.


## How to Run 
- Clone the application
- run npm run shopify app dev
  and follow the instructions to connect shopify store and use the application


## How to use project to create new products in Shopify Store

**New Products**
 You can create new products to your shopify store by filling the form with the following information:
     - Title (required)
     - Description
     - Image
     - Tag
     - custom metafield with a key, and value. (required)

**Dashboard**
  You can see all the the products created and can also filter the items in the search bar using tags given for the products.



